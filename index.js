let firstNumber = 12
let secondNumber = 5
let total = 0

// ARITHMETIC OPERATORS
// Addition Operator - responsible for adding two or more numbers
total = firstNumber + secondNumber
console.log('Result of addition operator is ' + total)

// Subtraction Operator - responsible for the subtraction of two or more numbers
total = firstNumber - secondNumber
console.log('Result of subtraction operator is ' + total)

// Multiplication Operator - responsible for the multiplying two or more numbers
total = firstNumber * secondNumber
console.log('Result of multiplication operator is ' + total)

// Division Operator - responsible for the dividing two or more numbers
total = firstNumber / secondNumber
console.log('Result of division operator is ' + total)

// Modulo Operator - responsible for getting the remainder of two or more numbers
total = firstNumber % secondNumber
console.log('Result of modulo operator is ' + total)


// ASSIGNMENT OPERATORS
// Reassignment Operator - should be a single equal sign, signifies re-assignment of new value into existing variable
total = 27
console.log(total)

// Addition Assignment Operator - uses the current value of the variable and adds a number to itself. Afterwards, reassigns it as the new value
total += 3
// total = total + 3
console.log('Result of addition assignment operator is ' + total)

// Subtraction Assignment Operator - uses the current value of the variable and adds a number to itself. Afterwards, reassigns it as the new value
total -= 5
// total = total - 5 
console.log('Result of subtraction assignment operator is ' + total)

// Addition Assignment Operator - uses the current value of the variable and adds a number to itself. Afterwards, reassigns it as the new value
total *= 4
// total = total * 4 
console.log('Result of multiplication assignment operator is ' + total)

// Division Assignment Operator - uses the current value of the variable and adds a number to itself. Afterwards, reassigns it as the new value
total /= 20
// total = total / 20 
console.log('Result of Division assignment operator is ' + total)


// MULTIPLE OPERATORS
let mdasTotal = 0
let pemdasTotal = 0

/*
When doing multiple operations, the program follows the MDAS rule.
MDAS (Multiplication then Division then Addition then Subtraction)
*/

mdasTotal = 2 + 1 - 5 * 4 / 1
console.log(mdasTotal)

/*
When doing multiple operations, the program follows the MDAS rule.
PEMDAS (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
Exponents are declared by using double asterisks ('**') after the number
*/

pemdasTotal = 5**2 + (10 - 2) / 2 * 3
console.log(pemdasTotal)

// INCREMENT AND DECREMENT OPERATORS
let incrementNumber = 1
let decrementNumber = 5

/*
	Pre-Increment - adds 1 first before reading value
	Pre-Decrement - subtracts 1 first before reading value
	
	Post-Increment - reads value first before adding 1
	Post-Decrement - reads value first before subtracting 1
*/ 
let resultOfPreIncrement = ++incrementNumber
let resultOfPreDecrement = --decrementNumber
let resultOfPostIncrement = incrementNumber++
let resultOfPostDecrement = decrementNumber--

console.log(resultOfPreIncrement)
console.log(resultOfPreDecrement)
console.log(resultOfPostIncrement)
console.log(resultOfPostDecrement)

// COERCION
// Coercion - When you add a string value to a non-string value
let a = '10'
let b = 10

console.log(a + b)

// Non-coercion - When you add a two or more non-string values
let d = 10
let f = 10

console.log(d + f)

// Type of Keyword - returns the data type of a variable
let stringType = 'hello'
let numberType = 1
let boolenType = true
let arrayType = ['1', '2', '3']
let objectType = {
	objectKey: 'Object Value'
}

console.log(typeof stringType)
console.log(typeof numberType)
console.log(typeof booleanType)
console.log(typeof arrayType)
console.log(typeof objectType)

// Computer reads 'true' as 1
// Computer reads 'false' as 0
console.log(true + 1)
console.log(false + 1)

// COMPARISON OPERATORS
// Equality Operator - checks if both values are the same, returns true if is.
console.log(5 == 5)
console.log('hello' == 'hello')
console.log(2 == '2')

// Strict Equality Operator - checks if both values AND data types are the same, returns true if it is.
console.log(2 === '2')
console.log(true === 1)

// Inequality Operator - checks if both values are NOT equal, returns true if they aren't
console.log(5 != 5)
console.log('hello' != 'hello')
console.log(2 != '2')

// Strict Inequality Operator - checks if both values AND data types are NOT equal, return true if they aren't.
console.log(1 !== '1')


// RELATIONAL OPERATORS
let firstVariable = 5
let secondVariable = 5

// Greater than / GT Operator checks if first value is greater than the second
console.log(firstVariable > secondVariable)
// Less than / LT Operator checks if first value is less than the second
console.log(firstVariable < secondVariable)

// Greater than or equal / GTE Operator checks if first value is either greater than OR equal to the second
console.log(firstVariable >= secondVariable)
// Less than or equal / LTE Operator checks if first value is either greater than OR equal to the second
console.log(firstVariable <= secondVariable)


// LOGICAL OPERATORS
let isLegalAge = true
let isRegistered = false

// AND Operator - Returns true if both statements are true. Returns fales if one if them is not true
console.log(isLegalAge && isRegistered)

// OR Operator - Returns true if only one of the statements are true. Returns fales if NONE of the statements are not true
console.log(isLegalAge || isRegistered)

// NOT Operator - Reverses the boolean value (from true to false vice-versa)
console.log(!isLegalAge)

// Test
console.log(isLegalAge && !isRegistered)
console.log(!isLegalAge || isRegistered)

// Truthy and Falsey values
// Everything that is either empty, zero, or null will equate to false, and everything that has some sort of value will equate to true
console.log([] == false)
console.log('' == false)
console.log(0 == false)

let userLoggedIn= 'Earl Diaz'







